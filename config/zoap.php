<?php

use App\Services\VirtualWallet\VirtualWalletService;
use App\Types\VirtualWallet\Client;
use App\Types\VirtualWallet\Product;

return [
    
    // Service configurations.

    'services'          => [
        'virtual-wallet'              => [
            'name'              => 'Virtual Wallet',
            'class'             => VirtualWalletService::class,
            'exceptions'        => [
                Exception::class
            ],
            'types'             => [
                'product'           => Product::class,
                'client'            => Client::class,
            ],
            'strategy'          => 'ArrayOfTypeComplex',
            'headers'           => [
                'Cache-Control'     => 'no-cache, no-store'
            ],
            'options'           => [
//                'soap_version'  => 2
            ]
        ],
        'demo'              => [
            'name'              => 'Demo',
            'class'             => 'Viewflex\Zoap\Demo\DemoService',
            'exceptions'        => [
                'Exception'
            ],
            'types'             => [
                'keyValue'          => 'Viewflex\Zoap\Demo\Types\KeyValue',
                'product'           => 'Viewflex\Zoap\Demo\Types\Product',
            ],
            'strategy'          => 'ArrayOfTypeComplex',
            'headers'           => [
                //'Cache-Control'     => 'no-cache, no-store'
            ],
            'options'           => []
        ]
        
    ],

    
    // Log exception trace stack?

    'logging'       => true,

    
    // Mock credentials for demo.

    'mock'          => [
        'user'              => 'test@test.com',
        'password'          => 'tester',
        'token'             => 'tGSGYv8al1Ce6Rui8oa4Kjo8ADhYvR9x8KFZOeEGWgU1iscF7N2tUnI3t9bX'
    ],

    
];
