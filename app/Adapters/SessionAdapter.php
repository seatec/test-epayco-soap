<?php

namespace App\Adapters;

use App\Models\Payment;
use App\Types\VirtualWallet\WalletSession as WalletSessionDto;

class SessionAdapter
{
    static function parseToDto(Payment $payment): WalletSessionDto
    {
        return new WalletSessionDto(
            $payment->session_id
        );
    }
}