<?php

namespace App\Adapters;

use App\Models\Wallet;
use App\Types\VirtualWallet\Wallet as WalletDto;

class WalletAdapter
{
    static function parseToDto(Wallet $wallet): WalletDto
    {
        return new WalletDto(
            $wallet->client_id,
            $wallet->balance,
        );
    }

    static function parseToArray(Wallet $wallet): array
    {
        return $wallet->toArray();
    }
}