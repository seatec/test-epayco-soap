<?php

namespace App\Adapters;

use App\Models\Client;
use App\Types\VirtualWallet\Client as ClientDto;

class ClientAdapter
{
    static function parseToDto(Client $client): ClientDto
    {
        return new ClientDto(
            $client->document,
            $client->name,
            $client->email,
            $client->mobile_number,
        );
    }

    static function parseToArray(Client $client): array
    {
        return $client->toArray();
    }
}