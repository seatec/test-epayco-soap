<?php

namespace App\Types\VirtualWallet;

class BalanceFilters
{
    /**
     * @var string
     */
    public string $document = '';

    /**
     * @var string
     */
    public string $mobile_number = '';

    /**
     * Balance Filters constructor.
     *
     * @param string $document
     * @param string $mobile_number
     */
    public function __construct(
        string $document = '',
        string $mobile_number = ''
    )
    {
        $this->document = $document;
        $this->mobile_number = $mobile_number;
    }
}