<?php

namespace App\Types\VirtualWallet;

class RechargeWallet
{
    /**
     * @var string
     */
    public string $document = '';

    /**
     * @var string
     */
    public string $mobile_number = '';

    /**
     * @var float
     */
    public float $amount = 0;

    /**
     * RechargeWallet constructor.
     *
     * @param string $document
     * @param string $mobile_number
     * @param float $amount
     */
    public function __construct(
        string $document = '',
        string $mobile_number = '',
        float $amount = 0
    )
    {
        $this->document = $document;
        $this->mobile_number = $mobile_number;
        $this->amount = $amount;
    }
}