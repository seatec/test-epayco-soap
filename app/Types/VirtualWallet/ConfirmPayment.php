<?php

namespace App\Types\VirtualWallet;

class ConfirmPayment
{
    /**
     * @var string
     */
    public string $session_id = '';

    /**
     * @var string
     */
    public string $token = '';

    /**
     * ConfirmPayment constructor.
     *
     * @param string $session_id
     * @param string $token
     */
    public function __construct(
        string $session_id = '',
        string $token = '',
    )
    {
        $this->session_id = $session_id;
        $this->token = $token;
    }
}