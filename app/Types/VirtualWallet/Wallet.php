<?php

namespace App\Types\VirtualWallet;

class Wallet
{
    /**
     * @var int
     */
    public int $client_id = 0;

    /**
     * @var float
     */
    public float $balance = 0;

    /**
     * Client constructor.
     *
     * @param int $client_id
     * @param float $balance
     */
    public function __construct(
        int $client_id = 0,
        float $balance = 0,
    )
    {
        $this->client_id = $client_id;
        $this->balance = $balance;
    }
}