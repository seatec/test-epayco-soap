<?php

namespace App\Types\VirtualWallet;

class Client
{
    /**
     * @var string
     */
    public string $document = '';

    /**
     * @var string
     */
    public string $name = '';

    /**
     * @var string
     */
    public string $email = '';

    /**
     * @var string
     */
    public string $mobile_number = '';

    /**
     * Client constructor.
     *
     * @param string $document
     * @param string $name
     * @param string $email
     * @param string $mobile_number
     */
    public function __construct(
        string $document = '',
        string $name = '',
        string $email = '',
        string $mobile_number = ''
    )
    {
        $this->document = $document;
        $this->name = $name;
        $this->email = $email;
        $this->mobile_number = $mobile_number;
    }
}