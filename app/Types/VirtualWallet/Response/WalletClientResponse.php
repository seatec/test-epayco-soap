<?php

namespace App\Types\VirtualWallet\Response;

use App\Types\VirtualWallet\Client;

class WalletClientResponse extends WalletResponse
{
    /**
     * @var \App\Types\VirtualWallet\Client $data
     */
    public ?Client $data = null;

    /**
     * WalletClientResponse constructor.
     *
     * @param bool $success
     * @param int $code
     * @param string $message
     * @param Client|null $data
     */
    public function __construct(
        bool $success = true,
        int $code = 200,
        string $message = '',
        Client $data = null
    ) {
        parent::__construct(
            $success,
            $code,
            $message,
        );
        $this->data = $data;
    }
}