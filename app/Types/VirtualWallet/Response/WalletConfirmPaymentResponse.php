<?php

namespace App\Types\VirtualWallet\Response;

use App\Types\VirtualWallet\WalletSession;

class WalletConfirmPaymentResponse extends WalletResponse
{
    /**
     * @var \App\Types\VirtualWallet\WalletSession $data
     */
    public ?WalletSession $data = null;

    /**
     * WalletConfirmPaymentResponse constructor.
     *
     * @param bool $success
     * @param int $code
     * @param string $message
     * @param WalletSession|null $data
     */
    public function __construct(
        bool $success = true,
        int $code = 200,
        string $message = '',
        WalletSession $data = null
    ) {
        parent::__construct(
            $success,
            $code,
            $message,
        );
        $this->data = $data;
    }
}