<?php

namespace App\Types\VirtualWallet\Response;

use App\Types\VirtualWallet\Client;
use App\Types\VirtualWallet\Wallet;

class WalletCheckBalanceResponse extends WalletResponse
{
    /**
     * @var \App\Types\VirtualWallet\Wallet $data
     */
    public ?Wallet $data = null;

    /**
     * WalletCheckoutResponse constructor.
     *
     * @param bool $success
     * @param int $code
     * @param string $message
     * @param Wallet|null $data
     */
    public function __construct(
        bool $success = true,
        int $code = 200,
        string $message = '',
        Wallet $data = null
    ) {
        parent::__construct(
            $success,
            $code,
            $message,
        );
        $this->data = $data;
    }
}