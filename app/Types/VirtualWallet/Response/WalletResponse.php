<?php

namespace App\Types\VirtualWallet\Response;

class WalletResponse
{
    /**
     * @var bool
     */
    public bool $success = false;

    /**
     * @var int
     */
    public int $code = 200;

    /**
     * @var string
     */
    public string $message = '';

    /**
     * WalletResponse constructor.
     *
     * @param bool $success
     * @param int $code
     * @param string $message
     */
    public function __construct(
        bool $success = true,
        int $code = 200,
        string $message = '',
    ) {
        $this->success = $success;
        $this->code = $code;
        $this->message = $message;
    }

}