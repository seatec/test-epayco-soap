<?php

namespace App\Types\VirtualWallet;

class WalletSession
{
    /**
     * @var string
     */
    public string $session_id = '';

    /**
     * SessionId constructor.
     *
     * @param string $session_id
     */
    public function __construct(
        string $session_id = '',
    )
    {
        $this->session_id = $session_id;
    }
}