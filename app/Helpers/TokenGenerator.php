<?php

use Illuminate\Support\Facades\Http;

if (! function_exists('tokenGenerator')) {
    /**
     * Token Generator
     *
     * @param int $length
     * @return string
     */
    function tokenGenerator(int $length): string
    {
        $token = '';
        for ($i = 0; $i < $length; $i++)
        {
            $token .= mt_rand(1, 9);
        }
        return $token;
    }
}

if (! function_exists('sendTokenToEmail')) {
    /**
     * Token Generator
     *
     * @param string $email
     * @param string $token
     * @return bool
     */
    function sendTokenToEmail(string $email, string $token): bool
    {
        try {
            Http::asForm()->post(config('wallet.email_token'), [
                'token' => $token,
                'email' => $email,
            ]);
        } catch (Exception $e) {
            return false;
        }
        return true;
    }
}