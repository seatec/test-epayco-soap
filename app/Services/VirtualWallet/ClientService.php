<?php

namespace App\Services\VirtualWallet;

use App\Adapters\ClientAdapter;
use App\Models\Client;
use App\Models\Transaction;
use Illuminate\Support\Facades\DB;
use Exception;
use stdClass;
use Symfony\Component\HttpFoundation\Response;

class ClientService
{

    public function registerClient(stdClass $client): array
    {
        $data = null;
        DB::beginTransaction();
        try {

            $exists = Client::firstWhere('document', $client->document);

            if ($exists) {
                return [
                    'success' => false,
                    'code' => Response::HTTP_CONFLICT,
                    'message' => "Client already exists"
                ];
            }

            $exists = Client::firstWhere('email', $client->email);

            if ($exists) {
                return [
                    'success' => false,
                    'code' => Response::HTTP_CONFLICT,
                    'message' => "Client email already exists"
                ];
            }

            $data = Client::create([
                'document' => $client->document,
                'name' => $client->name,
                'email' => $client->email,
                'mobile_number' => $client->mobile_number,
            ]);

            $data->wallet()->create();

            Transaction::create([
                'wallet_id' => $data->wallet->id,
                'type' => 'CREATE WALLET',
                'value' => $data->wallet->balance
            ]);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return [
                'success' => false,
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message' => "Client couldn't be created"
            ];
        }
        return [
            'success' => true,
            'code' => Response::HTTP_OK,
            'message' => 'Client created',
            'data' => ClientAdapter::parseToDto($data)
        ];
    }

}