<?php

namespace App\Services\VirtualWallet;

use App\Adapters\WalletAdapter;
use App\Models\Transaction;
use App\Models\Wallet;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use stdClass;
use Symfony\Component\HttpFoundation\Response;

class WalletService
{

    public function checkBalance(stdClass $data): array
    {
        $wallet = Wallet::whereHas('client', function ($query) use ($data) {
            $query
                ->where('document', '=', $data->document)
                ->where('mobile_number', '=', $data->mobile_number);
            })->first();

        if (!$wallet) {
            return [
                'success' => false,
                'code' => Response::HTTP_NOT_FOUND,
                'message' => "Wallet doesn't exist"
            ];
        }

        Transaction::create([
            'wallet_id' => $wallet->id,
            'type' => 'CHECK BALANCE',
            'value' => $wallet->balance
        ]);

        return [
            'success' => true,
            'code' => Response::HTTP_OK,
            'message' => 'Wallet found',
            'data' => WalletAdapter::parseToDto($wallet)
        ];
    }

    public function rechargeWallet(stdClass $data): array
    {
        $wallet = Wallet::whereHas('client', function ($query) use ($data) {
            $query
                ->where('document', '=', $data->document)
                ->where('mobile_number', '=', $data->mobile_number);
            })->first();

        if (!$wallet) {
            return [
                'success' => false,
                'code' => Response::HTTP_NOT_FOUND,
                'message' => "Wallet doesn't exist"
            ];
        }

        DB::beginTransaction();
        try {
            Transaction::create([
                'wallet_id' => $wallet->id,
                'type' => 'RECHARGE BALANCE',
                'value' => $wallet->balance
            ]);
            $wallet->balance += $data->amount;
            $wallet->save();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return [
                'success' => false,
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message' => "Wallet couldn't be recharged"
            ];
        }

        return [
            'success' => true,
            'code' => Response::HTTP_OK,
            'message' => 'Wallet recharged successfully',
            'data' => WalletAdapter::parseToDto($wallet)
        ];
    }

}