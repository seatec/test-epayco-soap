<?php

namespace App\Services\VirtualWallet;

use App\Types\VirtualWallet\Response\WalletClientResponse;
use App\Types\VirtualWallet\Response\WalletCheckBalanceResponse;
use App\Types\VirtualWallet\Response\WalletConfirmPaymentResponse;
use App\Types\VirtualWallet\Response\WalletResponse;
use stdClass;
use Symfony\Component\HttpFoundation\Response;
use App\Services\Validation\ValidationService;

class VirtualWalletService
{

    /**
     * @var ClientService $clientService
     */
    private ClientService $clientService;

    /**
     * @var WalletService $walletService
     */
    private WalletService $walletService;

    /**
     * @var PaymentService $paymentService
     */
    private PaymentService $paymentService;

    public function __construct()
    {
        $this->clientService = new ClientService();
        $this->walletService = new WalletService();
        $this->paymentService = new PaymentService();
    }

    /**
     * Test
     *
     * @param int $productId
     * @return array
     */
    public function test(int $productId): array
    {
        return ['status' => 'true', 'message' => "It's alive"];
    }

    /**
     * Registro de Clientes
     *
     * @param \App\Types\VirtualWallet\Client $client
     * @return \App\Types\VirtualWallet\Response\WalletClientResponse
     */
    public function registroClientes(stdClass $client): WalletClientResponse
    {
        $validation = ValidationService::validate($client, [
            'document' => 'required',
            'name' => 'required',
            'email' => 'required|email',
            'mobile_number' => 'required'
        ]);

        if (!$validation['success']) {
            header("Status: " . Response::HTTP_BAD_REQUEST);
            return new WalletClientResponse(
                $validation['success'],
                Response::HTTP_BAD_REQUEST,
                $validation['error'],
                null
            );
        }
        $response = $this->clientService->registerClient($client);
        if ($response['code'] != Response::HTTP_OK) {
            $code = $response['code'];
            header("Status: {$code}");
        }
        return new WalletClientResponse(
            $response['success'],
            $response['code'],
            $response['message'],
        $response['data'] ?? null
        );
    }

    /**
     * Consulta de Saldos
     *
     * @param \App\Types\VirtualWallet\BalanceFilters $filters
     * @return \App\Types\VirtualWallet\Response\WalletCheckBalanceResponse
     */
    public function consultaSaldo(stdClass $filters): WalletCheckBalanceResponse
    {
        $validation = ValidationService::validate($filters, [
            'document' => 'required',
            'mobile_number' => 'required'
        ]);

        if (!$validation['success']) {
            header("Status: " . Response::HTTP_BAD_REQUEST);
            return new WalletCheckBalanceResponse(
                $validation['success'],
                Response::HTTP_BAD_REQUEST,
                $validation['error'],
                null
            );
        }

        $response = $this->walletService->checkBalance($filters);
        if ($response['code'] != Response::HTTP_OK) {
            $code = $response['code'];
            header("Status: {$code}");
        }
        return new WalletCheckBalanceResponse(
            $response['success'],
            $response['code'],
            $response['message'],
            $response['data'] ?? null
        );
    }

    /**
     * Consulta de Saldos
     *
     * @param \App\Types\VirtualWallet\RechargeWallet $recharge
     * @return \App\Types\VirtualWallet\Response\WalletCheckBalanceResponse
     */
    public function recargaBilletera(stdClass $recharge): WalletCheckBalanceResponse
    {
        $validation = ValidationService::validate($recharge, [
            'document' => 'required',
            'mobile_number' => 'required',
            'amount' => 'required',
        ]);

        if (!$validation['success']) {
            header("Status: " . Response::HTTP_BAD_REQUEST);
            return new WalletCheckBalanceResponse(
                $validation['success'],
                Response::HTTP_BAD_REQUEST,
                $validation['error'],
                null
            );
        }

        $response = $this->walletService->rechargeWallet($recharge);
        if ($response['code'] != Response::HTTP_OK) {
            $code = $response['code'];
            header("Status: {$code}");
        }
        return new WalletCheckBalanceResponse(
            $response['success'],
            $response['code'],
            $response['message'],
            $response['data'] ?? null
        );
    }

    /**
     * Generar Pago
     *
     * @param \App\Types\VirtualWallet\GeneratePayment $pay
     * @return \App\Types\VirtualWallet\Response\WalletConfirmPaymentResponse
     */
    public function pagar(stdClass $pay): WalletConfirmPaymentResponse
    {
        $validation = ValidationService::validate($pay, [
            'document' => 'required',
            'mobile_number' => 'required',
            'amount' => 'required',
        ]);

        if (!$validation['success']) {
            header("Status: " . Response::HTTP_BAD_REQUEST);
            return new WalletConfirmPaymentResponse(
                $validation['success'],
                Response::HTTP_BAD_REQUEST,
                $validation['error'],
                null
            );
        }

        $response = $this->paymentService->generatePayment($pay);
        if ($response['code'] != Response::HTTP_OK) {
            $code = $response['code'];
            header("Status: {$code}");
        }
        return new WalletConfirmPaymentResponse(
            $response['success'],
            $response['code'],
            $response['message'],
            $response['data'] ?? null
        );
    }

    /**
     * Confirmar Pago
     *
     * @param \App\Types\VirtualWallet\ConfirmPayment $check
     * @return \App\Types\VirtualWallet\Response\WalletResponse
     */
    public function confirmaPago(stdClass $check): WalletResponse
    {
        $validation = ValidationService::validate($check, [
            'session_id' => 'required|uuid',
            'token' => 'required|digits:6',
        ]);

        if (!$validation['success']) {
            header("Status: " . Response::HTTP_BAD_REQUEST);
            return new WalletResponse(
                $validation['success'],
                Response::HTTP_BAD_REQUEST,
                $validation['error'],
            );
        }

        $response = $this->paymentService->confirmPayment($check);
        if ($response['code'] != Response::HTTP_OK) {
            $code = $response['code'];
            header("Status: {$code}");
        }
        return new WalletResponse(
            $response['success'],
            $response['code'],
            $response['message']
        );
    }
}