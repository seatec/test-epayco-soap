<?php

namespace App\Services\VirtualWallet;

use App\Adapters\SessionAdapter;
use App\Models\Payment;
use App\Models\Transaction;
use App\Models\Wallet;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;
use stdClass;

use function tokenGenerator;

class PaymentService
{

    public function generatePayment(stdClass $data): array
    {
        $wallet = Wallet::with('client')
            ->whereHas('client', function ($query) use ($data) {
                $query
                    ->where('document', '=', $data->document)
                    ->where('mobile_number', '=', $data->mobile_number);
            })->first();

        if (!$wallet) {
            return [
                'success' => false,
                'code' => Response::HTTP_NOT_FOUND,
                'message' => "Wallet doesn't exist"
            ];
        }

        DB::beginTransaction();
        try {
            $payment = Payment::create([
                'session_id' => Str::uuid(),
                'token' => tokenGenerator(6),
                'value' => $data->amount
            ]);
            $payment->client()->associate($wallet->client);
            $payment->save();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::info('ERROR: ' . $e->getMessage());
            return [
                'success' => false,
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message' => "Payment couldn't be created"
            ];
        }

        sendTokenToEmail($payment->client->email, $payment->token);

        return [
            'success' => true,
            'code' => Response::HTTP_OK,
            'message' => 'An email with a token of 6 digits has been sent to your email',
            'data' => SessionAdapter::parseToDto($payment)
        ];
    }

    public function confirmPayment(stdClass $data): array
    {
        $payment = Payment::where('session_id', '=', $data->session_id)
            ->where('token', '=', $data->token)
            ->where('status', '=', 'started')
            ->first();

        if (!$payment) {
            return [
                'success' => false,
                'code' => Response::HTTP_NOT_FOUND,
                'message' => "Payment doesn't exist or was rejected before"
            ];
        }

        $wallet = $payment->client->wallet;

        if ($payment->value > $wallet->balance) {
            $payment->status = 'rejected';
            $payment->save();
            return [
                'success' => false,
                'code' => Response::HTTP_OK,
                'message' => "Payment amount is superior than the wallet's balance"
            ];
        }

        DB::beginTransaction();
        try {

            Transaction::create([
                'wallet_id' => $wallet->id,
                'type' => 'PAYMENT',
                'value' => -$payment->value
            ]);

            $wallet->balance -= $payment->value;
            $wallet->save();
            $payment->status = 'approved';
            $payment->save();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return [
                'success' => false,
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message' => "Payment couldn't be approved"
            ];
        }

        return [
            'success' => true,
            'code' => Response::HTTP_OK,
            'message' => 'Payment was approved successfully',
        ];
    }
}