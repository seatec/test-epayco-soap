<?php

namespace App\Services\Validation;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use stdClass;

class ValidationService
{
    /**
     * @param stdClass $data
     * @param array $rules
     * @return array
     */
    public static function validate(stdClass $data, array $rules): array
    {
        $payload = static::toArray($data);
        $validator = Validator::make($payload, $rules);
        if ($validator->stopOnFirstFailure()->fails()) {
            return [
                'success' => false,
                'error' => implode(", ", $validator->errors()->all())
            ];
        }
        return ['success' => true];
    }

    /**
     * @param stdClass $data
     * @return array
     */
    private static function toArray(stdClass $data): array
    {
        return json_decode(json_encode($data), true);
    }
}