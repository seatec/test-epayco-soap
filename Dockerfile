FROM composer:2.4.3 as build
FROM php:8.1-apache-buster as production

ENV APP_ENV=production
ENV APP_DEBUG=true

RUN docker-php-ext-configure opcache --enable-opcache && \
    docker-php-ext-install pdo pdo_mysql
COPY docker/php/conf.d/opcache.ini /usr/local/etc/php/conf.d/opcache.ini

COPY --from=build /app /var/www/html
COPY --from=build /usr/bin/composer /usr/bin/composer
COPY docker/000-default.conf /etc/apache2/sites-available/000-default.conf
# COPY .env.prod /var/www/html/.env

RUN apt-get update && apt-get install -y  \
    zlib1g-dev \
    libicu-dev \
    g++ \
    libssl-dev \
    apt-transport-https \
    gnupg \
    curl \
    gnupg2 \
    wget \
    ca-certificates \
    lsb-release \
    software-properties-common \
    libxml2-dev git \
    libpng-dev \
    libonig-dev \
    libzip-dev \
    zip \
    unzip \
    libgss3 \
    odbcinst \
    locales

RUN docker-php-ext-configure zip &&  \
    docker-php-ext-install \
    soap  \
    zip \
    mbstring

# RUN composer install --prefer-dist --no-dev --optimize-autoloader --no-interaction

#RUN php artisan key:generate && \
#    php artisan config:cache && \
#    php artisan route:cache && \
# RUN chmod 777 -R /var/www/html/storage/ && \
#    chown -R www-data:www-data /var/www/ && \

RUN sed -i "s|DEFAULT@SECLEVEL=2|DEFAULT@SECLEVEL=1|g" /usr/lib/ssl/openssl.cnf
RUN    a2enmod rewrite


